export const GET_FILM_LIST = 'GET_FILM_LIST';
export const GET_FILM_LIST_SUCCESS = 'GET_FILM_LIST_SUCCESS';
export const GET_FILM_LIST_FAIL = 'GET_FILM_LIST_FAIL';
export const VIEW_SPECIFIC_FILM = 'VIEW_SPECIFIC_FILM';
export const VIEW_SPECIFIC_FILM_SUCCESS = 'VIEW_SPECIFIC_FILM_SUCCESS';
export const VIEW_SPECIFIC_FILM_FAIL = 'VIEW_SPECIFIC_FILM_FAIL';
export const GET_CHARACTER = 'GET_CHARACTER';
export const GET_CHARACTER_SUCCESS = 'GET_CHARACTER_SUCCESS';
export const GET_CHARACTER_FAIL = 'GET_CHARACTER_FAIL';
export const GET_VEHICLE = 'GET_VEHICLE';
export const GET_VEHICLE_SUCCESS = 'GET_VEHICLE_SUCCESS';
export const GET_VEHICLE_FAIL = 'GET_VEHICLE_FAIL';
export const GET_STARSHIP = 'GET_STARSHIP';
export const GET_STARSHIP_SUCCESS = 'GET_STARSHIP_SUCCESS';
export const GET_STARSHIP_FAIL = 'GET_STARSHIP_FAIL';
export default function reducer(state = {
  getFilmListState: {},
  viewSpecificFilmState: {},
  getCharacterState: {},
  getVehicleState:{},
  getStarShipState:{},
}, action) {
  switch (action.type) {

    case GET_FILM_LIST:
      return { ...state, loading: true };
    case GET_FILM_LIST_SUCCESS:
      return { ...state, loading: false, getFilmListState: action.payload.data };
    case GET_FILM_LIST_FAIL:
      console.log(action)
      return { ...state, loading: false, getFilmListState: action.error.response.data };

    case VIEW_SPECIFIC_FILM:
      return { ...state, loading: true };
    case VIEW_SPECIFIC_FILM_SUCCESS:
      return { ...state, loading: false, viewSpecificFilmState: action.payload.data };
    case VIEW_SPECIFIC_FILM_FAIL:
      return { ...state, loading: false, viewSpecificFilmState: action.error.response.data };

    case GET_CHARACTER:
      return { ...state, loading: true };
    case GET_CHARACTER_SUCCESS:
      return { ...state, loading: false, getCharacterState: action.payload.data };
    case GET_CHARACTER_FAIL:
      return { ...state, loading: false, getCharacterState: action.error.response.data };

    case GET_VEHICLE:
      return { ...state, loading: true };
    case GET_VEHICLE_SUCCESS:
      return { ...state, loading: false, getVehicleState: action.payload.data };
    case GET_VEHICLE_FAIL:
      return { ...state, loading: false, getVehicleState: action.error.response.data };

    case GET_STARSHIP:
      return { ...state, loading: true };
    case GET_STARSHIP_SUCCESS:
      return { ...state, loading: false, getStarShipState: action.payload.data };
    case GET_STARSHIP_FAIL:
      return { ...state, loading: false, getStarShipState: action.error.response.data };
        
    default:
      return state;
  }
}

export function getFilmList() {
  return {
    type: GET_FILM_LIST,
    payload: {
      request: {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
        url: `/films`,
      }
    }
  };
}

export function viewSpecificFilm(id) {
  return {
    type: VIEW_SPECIFIC_FILM,
    payload: {
      request: {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
        url: `/films/${id}`,
      }
    }
  };
}


export function getCharacter(id) {
  return {
    type: GET_CHARACTER,
    payload: {
      request: {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
        url: `/people/${id}`,
      }
    }
  };
}

export function getVehicle(id) {
  return {
    type: GET_VEHICLE,
    payload: {
      request: {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
        url: `/vehicles/${id}`,
      }
    }
  };
}

export function getStarship(id) {
  return {
    type: GET_STARSHIP,
    payload: {
      request: {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
        url: `/starships/${id}`,
      }
    }
  };
}
